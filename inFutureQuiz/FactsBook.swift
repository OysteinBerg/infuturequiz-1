//
//  FactsBook.swift
//  inFutureQuiz
//
//  Created by Øystein Tetlie Berg on 10.12.14.
//  Copyright (c) 2014 inFuture AS. All rights reserved.
//

import Foundation


struct FactBook {
    
    let factsStatementArray = [
        "spiller i band?",
        "spilte i band med Jokke?",
        "har gitt ut filmquizbok?",
        "er lidenskapelig sopp-plukker?",
        "har opplevd et jordsjelv på 6,7?",
        "driver med paragliding?",
        "brettseiler året rundt?",
        "fridde i sovepose?",
        "har ´As Good as it Gets´ som favorittfilm?",
        "har bodd i fem leiligheter i samme gate ila sitt liv?",
        "er halvt fransk?",
        "har bursdag 7. mai?",
        "har vært student i Zürich?",
        "kommer fra Østfold?",
        "har bursdag 7. oktober?",
        "har jobbet i legemiddelindustrien?",
        "var med på å unnfange Grandiosaen?",
        "har jakthund?",
        "er født i Afrika?",
        "har bursdag 6. august?",
        "kjørte før en klassisk Porsche 911?",
        "har x-box på hytta?",
        "har spilt fotball i 10 år og fikk en gang bronsje i Norway Cup?",
        "har spilt cello i barndommen?",
        "har studert Molekylærbiologi?",
        "er kvart dansk?",
        "har flest kjøkkenmaskiner?",
        "har en fortid som lektor?",
        "har motorsag?",
        "har spilt sjakk aktivt?",
        "sitter i styret i et arkitektkontor?",
        "har et NM-gull i lagsjakk?",
        "er oppvokst på Oppsal?",
        "har bursdag 24. august?",
        "er æresmedlem av alumniforeningen sin fra NTNU?",
        "har rappet på NRK i best sendetid?",
        "har forsøkt kommersialisere digitale begravelser?",
        "har studert i Skottland?",
        "har kjøpt to leiligheter der begge har hatt vannlekasje?",
        "møtte kjæresten sin på Colonel Mustards?",
        "drar på overnattingstur ute en gang i måneden?",
        "har bursdag 5. oktober?",
        "synger Bob Marley hver kveld?",
        "spiller gitar - men bare i enerom?",
        "elsker tegnefilmer?"
    ]
    
    let factsPersonArray = [
            "Olav",
            "Nikolai",
            "Camilla",
            "Camilla",
            "Kari",
            "Erik",
            "Øystein",
            "Robert",
            "Bjørn",
            "Bjørn",
            "Bjørn",
            "Bjørn",
            "Bjørn",
            "Svein",
            "Svein",
            "Svein",
            "Svein",
            "Svein",
            "Olav",
            "Olav",
            "Olav",
            "Olav",
            "Kari",
            "Kari",
            "Kari",
            "Kari",
            "Camilla",
            "Camilla",
            "Camilla",
            "Nikolai",
            "Nikolai",
            "Nikolai",
            "Nikolai",
            "Robert",
            "Robert",
            "Robert",
            "Robert",
            "Erik",
            "Erik",
            "Erik",
            "Erik",
            "Øystein",
            "Øystein",
            "Øystein",
            "Øystein"
    ]
    
    func randomFact() -> (factStatement: String, factPerson: String) {
        var unsignedArrayCount = UInt32(factsStatementArray.count)
        var unsignedRandomNumber = arc4random_uniform(unsignedArrayCount)
        var randomNumber = Int(unsignedRandomNumber)
        
        return (factsStatementArray[randomNumber], factsPersonArray[randomNumber])
        
    }
}
