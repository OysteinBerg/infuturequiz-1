//
//  ViewController.swift
//  inFutureQuiz
//
//  Created by Øystein Tetlie Berg on 10.12.14.
//  Copyright (c) 2014 inFuture AS. All rights reserved.
//

import UIKit
import AVFoundation

var answerPerson: String = ""
var currentPerson: String = ""
var riktigSvar: String = "Riktig! Trykk neste!"
var feilSvar: String = "Feil! Riktig svar er "

var antallRiktige = 0
var antallSpm = 0

// definere variabler som skal holde de lydene som skal kunne spilles av
var nextSound = AVAudioPlayer()
var successSound = AVAudioPlayer()
var failureSound = AVAudioPlayer()

class ViewController: UIViewController {

    @IBOutlet var FactStatementLabel: UILabel!
    @IBOutlet var AnswerState: UILabel!
    @IBOutlet var RiktigeLabel: UILabel!
    @IBOutlet var BackgroundImage: UIImageView!
    
        let factBook = FactBook()
    
// sette opp en funksjon for lydavspilling
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer  {
        
        var path = NSBundle.mainBundle().pathForResource(file, ofType:type)
        var url = NSURL.fileURLWithPath(path!)
        
        var error: NSError?
        
        var audioPlayer:AVAudioPlayer?
        audioPlayer = AVAudioPlayer(contentsOfURL: url, error: &error)
        
        return audioPlayer!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
 
    // kode for å definere lyder som skal kunne spilles av
        nextSound = self.setupAudioPlayerWithFile("next", type:"wav")
        successSound = self.setupAudioPlayerWithFile("success1", type:"wav")
        failureSound = self.setupAudioPlayerWithFile("failure", type:"wav")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func NextFactButton(sender: UIButton) {
        var currentFact = factBook.randomFact()
        FactStatementLabel.text = currentFact.factStatement
        currentPerson = currentFact.factPerson
        AnswerState.text = "Svar nå!"
        antallSpm = antallSpm + 1
        BackgroundImage.image = UIImage(named: "siluetter.png")
        
        nextSound.play()
    }
    
    @IBAction func KariButton(sender: UIButton) {
        answerPerson = "Kari"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
    @IBAction func ButtonBjorn(sender: UIButton) {
        answerPerson = "Bjørn"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
    @IBAction func ErikButton(sender: AnyObject) {
        answerPerson = "Erik"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
    @IBAction func RobertButton(sender: UIButton) {
        answerPerson = "Robert"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
    @IBAction func OysteinButton(sender: UIButton) {
        answerPerson = "Øystein"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
    @IBAction func OlavButton(sender: UIButton) {
        answerPerson = "Olav"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
    @IBAction func CamillaButton(sender: UIButton) {
        answerPerson = "Camilla"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
    @IBAction func NikolaiButton(sender: UIButton) {
        answerPerson = "Nikolai"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
    @IBAction func SveinButton(sender: UIButton) {
        answerPerson = "Svein"
        if answerPerson == currentPerson {
            AnswerState.text = riktigSvar
            antallRiktige = antallRiktige + 1
            successSound.play()
        } else {
            AnswerState.text = feilSvar + currentPerson + "!"
            failureSound.play()
        }
        RiktigeLabel.text = String(antallRiktige) + " riktige av " + String(antallSpm) + " forsøk"
        BackgroundImage.image = UIImage(named: "\(currentPerson)-fullsize.png")
    }
    
}

